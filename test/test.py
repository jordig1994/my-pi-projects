 #!/usr/bin/env python

 import serial
 import time

 # /dev/ttyACM0 = Arduino Uno on Linux
 # /dev/ttyUSB0 = Arduino Duemilanove on Linux

 arduino = serial.Serial('/dev/ttyACM0', 9600)
 time.sleep(2) # waiting the initialization...

 arduino.write('H') # turns LED ON
 time.sleep(1) # waits for 1 second

 arduino.write('L') # turns LED OFF
 time.sleep(1) # waits for 1 s   :height: 100px
:width: 200 pxecond

 arduino.close() #say goodbye to Arduino